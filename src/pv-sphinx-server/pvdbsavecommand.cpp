#include "pvdbsavecommand.h"
#include "./pvmessageobserver.h"
#include "./pvdbmessage.h"
#include "pvappconfig.h"

extern PVAppConfig g_appConfig;

PVDbSaveCommand::PVDbSaveCommand(PVMessageObserver * lpReceiver, PVMessage * lpMsg)
    :PVBaseCommand(lpReceiver, "")
{
    m_lpMsg = dynamic_cast<PVDBMessage *>(lpMsg);
}

PVDbSaveCommand::~PVDbSaveCommand()
{
}

bool PVDbSaveCommand::Execute()
{
    if (m_lpMsg == NULL || g_appConfig.GetMySqlConnection() == NULL)
        return (false);

    Save2MySql(m_lpMsg->GetFields(), m_lpMsg->GetValues());
    return (true);
}

int PVDbSaveCommand::Save2MySql(std::string names, std::string values)
{
    std::string query;
    query += std::string("INSERT INTO ") + DB_RESULTS_TABLE + " " + names + " VALUES" + values;

    return(mysql_query(g_appConfig.GetMySqlConnection(), query.c_str()));
}
