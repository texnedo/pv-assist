#include "pvspeechrecognizertest.h"
#include "pvringbuffer.h"
#include "pvspeechrecognizer.h"
#include "pvappconfig.h"
#include <memory>
#include <sys/types.h>
#include <sys/stat.h>
#include <string>
#include <vector>

#define VOICE_BUFFER_PORTION_SIZE 128
#define MAX_WAITINGTIME 60
#define SAMPLE_RATE 8000

extern PVAppConfig g_appConfig;

CPPUNIT_TEST_SUITE_REGISTRATION( PVSpeechRecognizerTest );

void PVSpeechRecognizerTest::tearDown()
{
    fclose(m_file);
}

void PVSpeechRecognizerTest::setUp()
{
}

void PVSpeechRecognizerTest::RecognizeAndLogData()
{
    std::auto_ptr<PVRingBuffer> sharedBuffer(new PVRingBuffer(MAX_WAITINGTIME * SAMPLE_RATE * 2));
    std::string filePath = "../../../samples/test_from_pjsip/send_message.wav";

    struct stat outfilestatus;
    stat(filePath.c_str(), &outfilestatus );
    CPPUNIT_ASSERT(outfilestatus.st_size != 0);

    std::vector<short> input;
    m_file = fopen(filePath.c_str(), "r");

    input.resize(outfilestatus.st_size / 2);

    int bytes_read = fread(&input[0], 2, outfilestatus.st_size, m_file);

    if (bytes_read != outfilestatus.st_size / 2)
        CPPUNIT_FAIL("wrong file size");

    input.erase(input.begin(), input.begin() + 44);
    sharedBuffer->WriteData((char *)&input[0], input.size() * 2);

    //char * argv [] = {"-fwdflat", "no" "-samprate" "8000", "-lm", "/home/kvant/src/mitlm.0.4/test1.lm", "-dict", "/usr/share/pocketsphinx/model/lm/test_nsh/cmu07a.dic", "-hmm", "/usr/share/pocketsphinx/model/hmm/test_nsh/", "-nfft", "256", "-backtrace", "yes", "-bestpath", "yes", "-inbuffer", "no", "-infile", "/home/kvant/asterisk-voice-dump.wav" };
    //int argc = 19;

    std::auto_ptr<PVSpeechRecognizer> lpRecognizer(new PVSpeechRecognizer(g_appConfig.GetArgCount(),
                                                                          g_appConfig.GetArgValues(),
                                                                          VOICE_BUFFER_PORTION_SIZE,
                                                                          sharedBuffer.get(), NULL));

    lpRecognizer->Start();
    sleep(80);
}
