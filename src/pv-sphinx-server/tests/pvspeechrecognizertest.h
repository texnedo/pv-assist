#ifndef PVSPEECHRECOGNIZERTEST_H
#define PVSPEECHRECOGNIZERTEST_H

#include <cppunit/extensions/HelperMacros.h>

class PVSpeechRecognizerTest : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE( PVSpeechRecognizerTest );
    CPPUNIT_TEST( RecognizeAndLogData );
    CPPUNIT_TEST_SUITE_END();
public:
    void virtual tearDown();
    void virtual setUp();
private:
    FILE * m_file;
protected:
    void RecognizeAndLogData();
};

#endif // PVSPEECHRECOGNIZERTEST_H
