#ifndef PVSIPCLIENTTEST_H
#define PVSIPCLIENTTEST_H

#include <cppunit/extensions/HelperMacros.h>

class PVSipClientTest : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE( PVSipClientTest );
    CPPUNIT_TEST( ConnectAndLogData );
    CPPUNIT_TEST_SUITE_END();
public:
    void virtual tearDown();
    void virtual setUp();
protected:
    void ConnectAndLogData();
};

#endif // PVSIPCLIENTTEST_H
