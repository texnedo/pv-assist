#ifndef PVSIPCLIENTCONFIG_H
#define PVSIPCLIENTCONFIG_H

#include "../pjproject-2.0-beta/pjsip/include/pjsua-lib/pjsua.h"
#include <string>

/*Class for storing all configuration properties from pjsip, that necessary for normal operating of
pvsipclient class.*/
class PVSipClientConfig
{
public:
    //connectivity settings
    pjsua_config            cfg;
    pjsua_logging_config    log_cfg;
    pjsua_media_config	    media_cfg;
    pj_bool_t               no_refersub;
    pj_bool_t               ipv6;
    pj_bool_t               enable_qos;
    pj_bool_t               no_tcp;
    pj_bool_t               no_udp;
    pj_bool_t               use_tls;
    pjsua_transport_config  udp_cfg;
    pjsua_transport_config  rtp_cfg;
    pjsip_redirect_op	    redir_op;

    unsigned                acc_cnt;
    pjsua_acc_config	    acc_cfg;
    pjsua_acc_id            acc_id;

    //memory pool for pjsip library
    pj_pool_t		   *pool;

    //members for writing in memory buffer
    pjmedia_port            *mem_port;
    pjsua_conf_port_id      mem_slot;
    pjmedia_port            *no_sound_port;
    void                    *mem_buffer;

    //members for reading voice from memory
    pjmedia_port            *play_mem_port;
    pjsua_conf_port_id      play_mem_slot;
    void                    *play_mem_buffer;
    FILE                    *play_mem_file;

    //optionally dump all raw voice data to binary file
    FILE                    *buffer_dump_file;
    pj_bool_t               enable_log_dumping;
    std::string             log_dump_file_name;

public:
    PVSipClientConfig();
    bool SerializeConfig(std::string fileName);
    bool DeserializeConfig(std::string fileName);
};

#endif // PVSIPCLIENTCONFIG_H
