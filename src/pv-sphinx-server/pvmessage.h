#ifndef PVMESSAGE_H
#define PVMESSAGE_H

#include <time.h>

class PVMessage
{
public:
    enum PVMessageType
    {
        DEFAULT_MSG = 0,
        SIP_CONNECTED_MSG,
        SIP_DISCONNECTED_MSG,
        SIP_MEDIA_STARTED_MSG,
        SIP_MEDIA_STOPPED_MSG,
        TEXT_TO_SPEECH_MSG,
        RECOGNITION_RESULT_MSG,
        DB_TASK_MSG
    };
private:
    time_t m_timeStamp;
protected:
    PVMessageType m_typeId;
public:
    PVMessageType GetType(){return (m_typeId);}
    PVMessage();
    PVMessage(PVMessageType typeId);
    PVMessage(const PVMessage& copyMsg);
    PVMessage& operator=(const PVMessage& assignMsg);
    time_t GetTimeStamp() {return (m_timeStamp);}
    virtual ~PVMessage();
    virtual bool Execute();
};

#endif // PVMESSAGE_H
