#include "pvspeechrecognizedmessage.h"
#include <algorithm>

PVSpeechRecognizedMessage::PVSpeechRecognizedMessage(std::string sText)
    :PVText2SpeechMessage(sText)
{
    m_typeId = PVMessage::RECOGNITION_RESULT_MSG;
}

PVSpeechRecognizedMessage::~PVSpeechRecognizedMessage()
{
}

bool PVSpeechRecognizedMessage::ImportFromPocketSphinx(ps_nbest_ext_t *extList, int extListLength)
{
    if (extList == NULL || extListLength == 0)
        return (false);

    m_CandidateList.resize(extListLength);
    for(int i = 0; i < extListLength; i++)
    {
        NBestCandidate hyp;
        hyp.iScore = extList[i].total_score;

        //TODO - optimize this code for performance

        hyp.wordScore.reserve(extList[i].len);
        hyp.wordScore.assign(&extList[i].scores[0], &extList[i].scores[extList->len - 1]);
        std::reverse(hyp.wordScore.begin(), hyp.wordScore.end());

        hyp.wordSequence.resize(extList[i].len);
        hyp.sText = "";
        for (int stri = extList[i].len - 1; stri >= 0; stri--)
        {
            if (    extList[i].words[stri] != NULL &&
                    strlen(extList[i].words[stri]) != 0 &&
                    strcmp(extList[i].words[stri], "<s>") != 0 &&
                    strcmp(extList[i].words[stri], "</s>") != 0)
            {
                hyp.wordSequence[stri] = (char *)extList[i].words[stri];
                if (hyp.sText.size() == 0)
                    hyp.sText += (char *)extList[i].words[stri];
                else
                    hyp.sText += std::string(" ") + (char *)extList[i].words[stri];
            }
        }

        hyp.wordSequenceId.reserve(extList->len);
        hyp.wordSequenceId.assign(&extList[i].wordids[0], &extList[i].wordids[extList->len - 1]);
        std::reverse(hyp.wordSequenceId.begin(), hyp.wordSequenceId.end());

        m_CandidateList[i] = hyp;
    }
}
