#include "pvmessage.h"


PVMessage::PVMessage()
{
    m_typeId = DEFAULT_MSG;
    m_timeStamp = time(NULL);
}

PVMessage::PVMessage(PVMessage::PVMessageType typeId)
{
    m_typeId = typeId;
    m_timeStamp = time(NULL);
}

PVMessage::PVMessage(const PVMessage &copyMsg)
{
    m_typeId = copyMsg.m_typeId;
    m_timeStamp = copyMsg.m_timeStamp;
}

PVMessage &PVMessage::operator =(const PVMessage &assignMsg)
{
    m_typeId = assignMsg.m_typeId;
    m_timeStamp = assignMsg.m_timeStamp;
}

PVMessage::~PVMessage()
{
}

bool PVMessage::Execute()
{
    return (false);
}
