#ifndef PVSIPSTATEMESSAGE_H
#define PVSIPSTATEMESSAGE_H

#include "pvmessage.h"

class PVSipStateMessage : public PVMessage
{
public:
    PVSipStateMessage();
};

#endif // PVSIPSTATEMESSAGE_H
