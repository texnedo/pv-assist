#ifndef PVSPEECHSYNTHESIZER_H
#define PVSPEECHSYNTHESIZER_H

#include "pvmessageobserver.h"
#include <memory>
#include <pthread.h>
#include "pvrateconverter.h"

class PVFestivalClient;
class PVRingBuffer;

class PVSpeechSynthesizer  : public PVMessageObserver
{
private:
    std::auto_ptr<PVFestivalClient> m_synthClient;

    pthread_t m_connectionThread;
    static bool connectionDestroyEvent;
    static pthread_mutex_t connectionDestroyMutex;

    PVRingBuffer * m_lpExternalBuffer;

    static PVRateConverter rateConverter;

    virtual void OnMessage(std::tr1::shared_ptr<PVMessage> inMsg);
    static void * ConnectionThreadProc(void * lpSynthesizer);
    bool StartInternal();

    FILE * m_voiceDumpFile;
public:
    PVSpeechSynthesizer(PVRingBuffer *lpBuffer);
    virtual ~PVSpeechSynthesizer();
    bool Start();
};

#endif // PVSPEECHSYNTHESIZER_H
