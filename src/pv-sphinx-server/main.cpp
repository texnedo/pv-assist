#include <tr1/memory>

#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>

#include "pvappconfig.h"
#include "pvrootdispatcher.h"

//#define RUN_TESTS

PVAppConfig g_appConfig;

bool RunTests()
{
#ifdef RUN_TESTS
    // Create the event manager and test controller
    CPPUNIT_NS::TestResult controller;

    // Add a listener that colllects test result
    CPPUNIT_NS::TestResultCollector result;
    controller.addListener( &result );

    // Add a listener that print dots as test run.
    CPPUNIT_NS::BriefTestProgressListener progress;
    controller.addListener( &progress );

    // Add the top suite to the test runner
    CPPUNIT_NS::TestRunner runner;
    runner.addTest( CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest() );
    runner.run( controller );

    // Print test in a compiler compatible format.
    CPPUNIT_NS::CompilerOutputter outputter( &result, CPPUNIT_NS::stdCOut() );
    outputter.write();

    return result.wasSuccessful() ? 0 : 1;
#else
    return false;
#endif
}

void ShowConfigParams()
{
    std::string args = "-fwdflat no -samprate 8000 -lm /home/kvant/src/mitlm.0.4/test1.lm -dict /usr/share/pocketsphinx/model/lm/test_nsh/cmu07a.dic -hmm /usr/share/pocketsphinx/model/hmm/test_nsh/ -nfft 256 -backtrace yes   -bestpath yes -inbuffer no -infile /home/kvant/asterisk-voice-dump.wav -domain 192.168.0.56 -user testuser1 -pwd 685702 -calluri sip:1243@192.168.0.56 -rcvvoicedump ../test_data/sip_client_output8000.wav -sndvoicedump /home/kvant/voice-answer.wav -playfile ../test_data/send_message3.wav -runtests";
    //args = args.replace(' ', '\n');

    printf("Avalible arguments: \n%s\n", args.c_str());
}

int main(int argc, char *argv[])
{
    //configure global application settings
    if (!g_appConfig.ParseCmdLineArgs(argc, argv) || g_appConfig.GetParamsAmount() == 0)
    {
        ShowConfigParams();
        return (-1);
    }

#ifdef RUN_TESTS
    //execute intermediate run actions
    if (g_appConfig.IsRunTests())
    {
        RunTests();
        exit(0);
    }
#endif

    std::auto_ptr<PVRootDispatcher> serverApplication(new PVRootDispatcher());
    serverApplication->Start();

    return 0;
}

