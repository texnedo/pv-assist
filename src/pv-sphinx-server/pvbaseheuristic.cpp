#include "pvbaseheuristic.h"
#include "pvspeechrecognizedmessage.h"
#include "pvspeechrecognizer.h"

PVBaseHeuristic::PVBaseHeuristic(InternalLM *lpLM, float iProbability)
    :m_lpLM(lpLM),
     m_iProbability(iProbability)
{
}

PVFindLowCandidateHeuristic::PVFindLowCandidateHeuristic(InternalLM *lpLM)
    :PVBaseHeuristic(lpLM, 0.1)
{
}

bool PVFindLowCandidateHeuristic::Execute(PVSpeechRecognizedMessage *lpMsg)
{
    if (lpMsg == NULL)
        return (false);

    PVSpeechRecognizedMessage::CANDIDATELIST * list = lpMsg->GetCandidateList();
    for (PVSpeechRecognizedMessage::CANDIDATELIST::iterator it = list->begin(); it != list->end(); ++it)
        if (it->iScore < list->begin()->iScore)
            return (false);

    return (true);
}

PVNotEqualHeuristic::PVNotEqualHeuristic(InternalLM *lpLM)
    :PVBaseHeuristic(lpLM, 0.2)
{
}

bool PVNotEqualHeuristic::Execute(PVSpeechRecognizedMessage *lpMsg)
{
    if (lpMsg == NULL)
        return (false);

    PVSpeechRecognizedMessage::CANDIDATELIST * list = lpMsg->GetCandidateList();
    if (list->begin()->sText != lpMsg->GetText())
        return (false);
    else
        return (true);
}

PVNotInSourceLMHeuristic::PVNotInSourceLMHeuristic(InternalLM *lpLM)
    :PVBaseHeuristic(lpLM, 0.7)
{
}

bool PVNotInSourceLMHeuristic::Execute(PVSpeechRecognizedMessage *lpMsg)
{
    if (lpMsg == NULL)
        return (false);

    if (m_lpLM == NULL)
        return (false);

    if (m_lpLM->lmSearchNGrams.find(lpMsg->GetText()) == m_lpLM->lmSearchNGrams.end())
        return (false);
    else
        return (true);
}
