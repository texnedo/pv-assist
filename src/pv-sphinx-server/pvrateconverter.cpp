#include "pvrateconverter.h"

PVRateConverter::PVRateConverter(int iInputSampleRate, int iOutputSampleRate)
    :m_iInputSampleRate(iInputSampleRate),
      m_iOutputSampleRate(iOutputSampleRate)
{
    m_lpContext = av_resample_init(
           iOutputSampleRate,   // out rate
           iInputSampleRate,  // in rate
           16,     // filter length
           10,     // phase count
           0,      // linear FIR filter
            1.0 );  // cutoff frequency
}

bool PVRateConverter::Resample(const std::vector<short> & src, std::vector<short> & dest)
{
    if (m_lpContext == NULL)
        throw (INIT_LIBAV_ERROR); //we can't work in such circumstances

    if (src.size() == 0)
        return (false);

    int fract = 1;
    if (m_iInputSampleRate >= m_iOutputSampleRate)
    {
        fract = m_iInputSampleRate / m_iOutputSampleRate;
        if (fract == 0)
            return (false);

        dest.resize(src.size() / fract);

        int samples_consumed = 0;
        int samples_output = av_resample(
               m_lpContext,
               (short *)&dest[0],
               (short *)&src[0],
               &samples_consumed,
               src.size(),
               src.size() / fract, // in samples
               0 );

        dest.resize(samples_output);

        if (dest.size() != 0)
            return (true);
        else
            return (false);
    }
    else
    {
        //TODO - add this case support
        return (false);
    }
}

bool PVRateConverter::Resample(const std::vector<char> &src, std::vector<char> &dest)
{
    //TODO - remove this hack
    std::vector<short> src16, dest16;
    src16.resize(src.size() / 2);
    memcpy(&src16[0], &src[0], src.size());
    bool bRet = Resample(src16, dest16);
    dest.resize(dest16.size() * 2);
    memcpy(&dest[0], &dest16[0], dest.size());

    return (bRet);
}

PVRateConverter::~PVRateConverter()
{
    if (m_lpContext)
        av_resample_close(m_lpContext);
}
