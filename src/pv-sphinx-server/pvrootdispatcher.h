#ifndef PVROOTDISPATCHER_H
#define PVROOTDISPATCHER_H

#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <tr1/memory>

#include "pvmessageobserver.h"

class PVRingBuffer;
class PVSipClient;
class PVSpeechRecognizer;
class PVSpeechSynthesizer;
class PVHeuristicAnalyzer;

/*Main class of server application. In current model all other comonents of system should communicate via this
dispatcher by sending messages.*/
class PVRootDispatcher : public PVMessageObserver
{
private:
    std::auto_ptr<PVRingBuffer> m_sharedInputBuffer;
    std::auto_ptr<PVRingBuffer> m_sharedOutputBuffer;
    std::auto_ptr<PVSipClient> m_SipClient;
    std::auto_ptr<PVSpeechRecognizer> m_Recognizer;
    std::auto_ptr<PVSpeechSynthesizer> m_Synthesizer;
    std::auto_ptr<PVHeuristicAnalyzer> m_Analyser;

    virtual void OnMessage(std::tr1::shared_ptr<PVMessage> inMsg);
public:
    PVRootDispatcher();
    virtual ~PVRootDispatcher();
    void Start();
};

#endif // PVROOTDISPATCHER_H
