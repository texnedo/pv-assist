#include "pvgetweathercommand.h"
#include <curl.h>
#include <tr1/memory>
#include <vector>
#include "pvutils.h"
#include "pvtext2speechmessage.h"
#include "pvmessageobserver.h"
#include "string.h"

#define WEATHER_GET_URL "http://free.worldweatheronline.com/feed/weather.ashx?q=Moscow&format=csv&num_of_days=2&key=043d986045101646120105"

PVGetWeatherCommand::PVGetWeatherCommand(PVMessageObserver * lpReceiver)
    :PVBaseCommand(lpReceiver, "")
{
}

PVGetWeatherCommand::~PVGetWeatherCommand()
{
}

size_t CurlWriteFunc(void *ptr, size_t size, size_t nmemb, std::string * str)
{
    str->append((char*)ptr, (char*)ptr + size * nmemb);
    return(str->size());
}

bool PVGetWeatherCommand::Execute()
{
     CURLcode res;
     bool bResult = false;
     CURL * lpCurlInstance = NULL;
     //TODO - read documentation if we need to destroy and create curl for each request
     if (lpCurlInstance == NULL)
        lpCurlInstance = curl_easy_init();

     if(lpCurlInstance)
     {
         //std::vector<char> queryData;
         //queryData.resize(20000);
         std::string queryData;

         /* First set the URL that is about to receive our POST. This URL can
          just as well be a https:// URL if that is what should receive the
          data. */
         curl_easy_setopt(lpCurlInstance, CURLOPT_URL, WEATHER_GET_URL);
         curl_easy_setopt(lpCurlInstance, CURLOPT_WRITEDATA, &queryData);
         curl_easy_setopt(lpCurlInstance, CURLOPT_WRITEFUNCTION, CurlWriteFunc);

         /* Perform the request, res will get the return code */
         res = curl_easy_perform(lpCurlInstance);

         if(res == CURLE_OK)
         {
             char *ct;
             res = curl_easy_getinfo(lpCurlInstance, CURLINFO_CONTENT_TYPE, &ct);

             if((CURLE_OK == res) && ct/* && m_lpReceiver != NULL*/)
             {
                 printf("\n%s\n", ct);
                 //int len = strlen(ct);
                 //fflush(stdout);
                 WeatherData data;
                 ParseWeatherServerResponse(queryData, data);
                 std::tr1::shared_ptr<PVMessage> msg(
                             new PVText2SpeechMessage(CreateWeatherAnswer(data)));
                 m_lpReceiver->SendMessage(msg);

                 bResult = true;
             }
         }

         curl_easy_cleanup(lpCurlInstance);
     }

     return (bResult);
}

void PVGetWeatherCommand::ParseWeatherServerResponse(std::string response, PVGetWeatherCommand::WeatherData &weather)
{
    std::vector<std::string> tokens = pvutils::split(response, '\n');
    for (std::vector<std::string>::iterator it = tokens.begin(); it != tokens.end(); ++it)
    {
        if ((*it)[0] != '#')
        {
            std::vector<std::string> w_tokens = pvutils::split(*it, ',');
            std::vector<std::string> date_tokens = pvutils::split(w_tokens[0], '-');
            if (date_tokens.size() == 3)
            {
                weather.date = w_tokens[0];
                weather.tempMaxC = w_tokens[1];
                weather.tempMinC = w_tokens[3];
                weather.windspeedKmph = w_tokens[6];
                break;
            }
        }
    }
}

std::string PVGetWeatherCommand::CreateWeatherAnswer(const PVGetWeatherCommand::WeatherData &weather)
{
    std::string answer = std::string("Current min temperature is ") + weather.tempMinC;
    return (answer);
}
