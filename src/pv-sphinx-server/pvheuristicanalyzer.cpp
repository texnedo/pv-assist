#include <stdio.h>
#include <stdlib.h>

#include "pvheuristicanalyzer.h"
#include "pvmessage.h"
#include "pvbaseheuristic.h"
#include "pvtext2speechmessage.h"
#include "pvspeechrecognizedmessage.h"
#include "pvspeechrecognizer.h"
#include "pvdbmessage.h"
#include "pvappconfig.h"

extern PVAppConfig g_appConfig;

#define DISCARD_PROBABILITY_THRESHOLD 0.5
#define DISCARD_ANSWER_TEXT "Please repeat command"

PVHeuristicAnalyzer::PVHeuristicAnalyzer(PVMessageObserver * lpReceiver, InternalLM *lm)
    :m_lpReceiver(lpReceiver),
     m_InternalRecognizerLM(new InternalLM(*lm))
{
    if (m_lpReceiver == NULL)
        throw(NULL_RECEIVER_POINTER_ERROR);

    if (m_InternalRecognizerLM.get() == NULL)
        throw(LM_INIT_ERROR);

    //create heuristic list
    m_CommandMap.push_back(std::tr1::shared_ptr<PVBaseHeuristic>(new PVNotEqualHeuristic(m_InternalRecognizerLM.get())));
    m_CommandMap.push_back(std::tr1::shared_ptr<PVBaseHeuristic>(new PVFindLowCandidateHeuristic(m_InternalRecognizerLM.get())));
    m_CommandMap.push_back(std::tr1::shared_ptr<PVBaseHeuristic>(new PVNotInSourceLMHeuristic(m_InternalRecognizerLM.get())));
}

void PVHeuristicAnalyzer::ClearResults()
{
    //TODO - may be we should remove this saving of result
    //for(HEURISTICLIST::iterator it = m_CommandMap.begin(); it != m_CommandMap.end(); it++)
    //    it->second.first = false;
}

void PVHeuristicAnalyzer::OnRecognitionResultMessage(PVSpeechRecognizedMessage * lpMsg)
{
    if (lpMsg == NULL)
        return;

    float iTotalProbability = 0;
    bool bHeuristicResult = false;
    for(HEURISTICLIST::iterator it = m_CommandMap.begin(); it != m_CommandMap.end(); it++)
    {
        PVBaseHeuristic * cmd = it->get();
        bHeuristicResult = cmd->Execute(lpMsg);

        //multiply command result with it's weight
        iTotalProbability += (float)bHeuristicResult * cmd->GetProbabiliry();
    }

    if (iTotalProbability < DISCARD_PROBABILITY_THRESHOLD)
    {
        //discard current command and try to listen it again
        std::tr1::shared_ptr<PVText2SpeechMessage> msg(new PVText2SpeechMessage(DISCARD_ANSWER_TEXT));
        m_lpReceiver->SendMessage(msg);
    }
    else
    {
        std::tr1::shared_ptr<PVText2SpeechMessage> msg(new PVText2SpeechMessage(lpMsg->GetText()));
        m_lpReceiver->SendMessage(msg);
    }

    std::tr1::shared_ptr<PVDBMessage> msgDb(new PVDBMessage(DB_RESULTS_TABLE));

    msgDb->AddParam(DB_USERTO_FIELD, g_appConfig.GetParamValue(SIP_USER_ARG));
    msgDb->AddParam(DB_USERFROM_FIELD, g_appConfig.GetParamValue(SIP_PLACE_CALL_ARG));
    msgDb->AddParam(DB_PHRASE_FIELD, lpMsg->GetText());

    char time [100];
    sprintf(time, "%d", msgDb->GetTimeStamp());
    msgDb->AddParam(DB_TIMESTAMP_FIELD, time);

    m_lpReceiver->SendMessage(msgDb);
}

void PVHeuristicAnalyzer::OnMessage(std::tr1::shared_ptr<PVMessage> inMsg)
{
    switch(inMsg->GetType())
    {
        case PVMessage::RECOGNITION_RESULT_MSG:
        {
            PVSpeechRecognizedMessage * lpMsg = dynamic_cast<PVSpeechRecognizedMessage *>(inMsg.get());
            OnRecognitionResultMessage(lpMsg);
            ClearResults();
            break;
        }
        default:
        {
            break;
        }
    }
}
