#include "pvappconfig.h"
#include <getopt.h>
#include <string.h>
#include <stdio.h>
#include "./pvutils.h"

PVAppConfig::PVAppConfig()
    :m_mySqlConnection(NULL)
{
}

PVAppConfig::~PVAppConfig()
{
    mysql_close(m_mySqlConnection);
}

bool PVAppConfig::CreateMySqlConnection()
{
    mysql_init(&m_mySql);

    if (HasParam("domain"))
    {
        std::string mysqlHost = GetParamValue("domain");
        m_mySqlConnection = mysql_real_connect(&m_mySql,
                mysqlHost.c_str(),
                USER,
                PASSWORD,
                INITDB,0,0,CLIENT_MULTI_RESULTS);
    }

    if (m_mySqlConnection == NULL)
    {
        printf("Problem with mysql connection: %s\n", mysql_error(&m_mySql));
        return (false);
    }
    else
        return (true);
}


bool PVAppConfig::ParseCmdLineArgs(int argc, char *argv[])
{
    m_argc = argc;
    m_argv = argv;

    for (int i=0; i<argc; i++)
        printf("%s\n", argv[i]);

    fflush(stdout);

    std::string name, value;
    for (int i = 1; i < argc; i++)
    {
        std::string token(argv[i]);
        token = pvutils::trim(token);
        if (token[0] == '-')
        {
            token.erase(token.begin());
            bool nextStep = false;
            if (name.size() != 0)
            {
                m_aParams.insert(std::make_pair(name, ""));
                nextStep = true;
            }
            name = token;
            if (!nextStep)
                continue;
        }
        else
            value = token;

        if (value.size() != 0 && name.size() != 0)
        {
            m_aParams.insert(std::make_pair(name, value));
            name = "";
            value = "";
        }
        else if (value.size() != 0 && name.size() == 0)
        {
            printf("Error in command line parsing\n");
            return (false);
        }
        else if (value.size() == 0 && name.size() != 0)
        {
            m_aParams.insert(std::make_pair(name, ""));
            name = "";
        }
        else if (value.size() != 0 && name.size() == 0)
        {
            printf("Error in command line parsing\n");
            return (false);
        }

    }

    if (value.size() == 0 && name.size() != 0)
    {
        m_aParams.insert(std::make_pair(name, ""));
        name = "";
    }
    else if (value.size() != 0)
    {
        printf("Error in command line parsing\n");
        return (false);
    }

    return (true);
//    int opt;
//    int option_index = 0;
//    static struct option long_options[] =
//    {
//        {TEST_RUNNING_ARG, 0, 0, 0},
//        {"backtrace", 0, 0, 0}
//    };

//    int c;
//    int digit_optind = 0;
//    int aopt = 0, bopt = 0;
//    char *copt = 0, *dopt = 0;

//    while (1)
//    {
//        opt = getopt_long (argc, argv, "", long_options, &option_index);
//        if (opt == -1)
//            break;

//        printf("%d\s", long_options[option_index].name);
//        int this_option_optind = optind ? optind : 1;
//        switch (opt)
//        {
//        case 0:
//            {
//                m_aParams.insert(std::make_pair(long_options[option_index].name, ""));
//                if( strcmp( TEST_RUNNING_ARG, long_options[option_index].name ) == 0 )
//                {
//                    m_bRunTests = true;
//                }
//                break;
//            }
//        case '0':
//        case '1':
//        case '2':
//            if (digit_optind != 0 && digit_optind != this_option_optind)
//              printf ("digits occur in two different argv-elements.\n");
//            digit_optind = this_option_optind;
//            printf ("option %c\n", c);
//            break;
//        case 'a':
//            printf ("option a\n");
//            aopt = 1;
//            break;
//        case 'b':
//            printf ("option b\n");
//            bopt = 1;
//            break;
//        case 'c':
//            printf ("option c with value '%s'\n", optarg);
//            copt = optarg;
//            break;
//        case 'd':
//            printf ("option d with value '%s'\n", optarg);
//            dopt = optarg;
//            break;
//        case '?':
//            break;
//        default:
//            printf ("?? getopt returned character code 0%o ??\n", c);
////        default:
////            {
////                break;
////            }
//        }
    //    }
}

MYSQL *PVAppConfig::GetMySqlConnection()
{
    if (m_mySqlConnection == NULL)
        CreateMySqlConnection();

    return (m_mySqlConnection);
}

std::string PVAppConfig::GetParamValue(std::string name)
{
    MAPSTR2STR::iterator it = m_aParams.find(name);
    if (it != m_aParams.end())
        return (it->second);
    else
        return ("");
}

bool PVAppConfig::HasParam(std::string name)
{
    MAPSTR2STR::iterator it = m_aParams.find(name);
    if (it != m_aParams.end())
        return (true);
    else
        return (false);
}
