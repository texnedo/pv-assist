#ifndef PVSPEECHRECOGNIZER_H
#define PVSPEECHRECOGNIZER_H

#include <sphinxbase/err.h>
#include <sphinxbase/ad.h>
#include <sphinxbase/cont_ad.h>
#include "../pocketsphinx-0.7/include/pocketsphinx.h"
#include <pthread.h>
#include "pvmessageobserver.h"
#include <tr1/memory>
#include <tr1/unordered_set>
#include <list>

class PVRingBuffer;

struct InternalLM
{
    std::list<std::string> lmUnigrams;
    std::list<std::string> lmBigrams;
    std::list<std::string> lmTrigrams;
    std::tr1::unordered_set<std::string> lmSearchNGrams;
};

class PVSpeechRecognizer : public PVMessageObserver
{
public:
    enum PVSpeechRecognizerError
    {
        WRONG_CONFIGUTATION = 0,
        SPHINX_INIT_ERROR,
        WRONG_BUFFER_SIZE,
        MEMORY_ALLOCATION_ERROR
    };
private:
    InternalLM m_InternalLM;
    ps_decoder_t * m_decoder;
    ad_rec_t * m_audioDevice;
    cont_ad_t * m_continiousListener;

    pthread_t m_connectionThread;
    static bool connectionDestroyEvent;
    static pthread_mutex_t connectionDestroyMutex;
    static bool QueryDestroyEvent();

    int m_iInternalBufferSize;
    static PVRingBuffer * lpExternalBuffer;

    PVMessageObserver * m_lpReceiver;

    static void * ConnectionThreadProc(void * lpRecognizer);

    //test stuff
    static FILE * m_testVoiceFile;
    static int32 AudioDeviceEmulationTestRead(ad_rec_t *handle, int16 *buf, int32 max);
    static int32 AudioDeviceBufferRead(ad_rec_t *handle, int16 *buf, int32 max);

    virtual void OnMessage(std::tr1::shared_ptr<PVMessage> inMsg);

    bool StartInternal();
    bool RecognizeCycle(bool bEnableInternalCycle);
    bool InitInternalLM(cmd_ln_t *config);
public:
    PVSpeechRecognizer(int argc, char *argv[], int iBufferSize, PVRingBuffer *lpBuffer, PVMessageObserver * lpReceiver);
    //TODO - next method to wrapper class and make pimpl architecture
    bool Start();
    InternalLM * GetSourceLM() {return (&m_InternalLM);}
    virtual ~PVSpeechRecognizer();
};

const arg_t cont_args_def[] = {
    POCKETSPHINX_OPTIONS,
    /* Argument file. */
    { "-argfile",
      ARG_STRING,
      NULL,
      "Argument file giving extra arguments." },
    { "-adcdev",
      ARG_STRING,
      NULL,
      "Name of audio device to use for input." },
    { "-infile",
      ARG_STRING,
      NULL,
      "Audio file to transcribe." },
    { "-time",
      ARG_BOOLEAN,
      "no",
      "Print word times in file transcription." },
    { "-inbuffer",
      ARG_BOOLEAN,
      "yes",
      "Recognize from memory buffer." },
    CMDLN_EMPTY_OPTION
};

#endif // PVSPEECHRECOGNIZER_H
