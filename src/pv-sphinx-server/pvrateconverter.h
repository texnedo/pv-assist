#ifndef PVRATECONVERTER_H
#define PVRATECONVERTER_H

extern "C"
{
#define __STDC_CONSTANT_MACROS // for UINT64_C
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
}
#include <vector>

class PVRateConverter
{
public:
   enum PVRateConverterError
   {
        INIT_LIBAV_ERROR
   };
private:
    AVResampleContext * m_lpContext;
    int m_iInputSampleRate;
    int m_iOutputSampleRate;
public:
    PVRateConverter(int iInputSampleRate, int iOutputSampleRate);
    bool Resample(const std::vector<short> & src, std::vector<short> & dest);
    ~PVRateConverter();
    bool Resample(const std::vector<char> & src, std::vector<char> & dest);
};

#endif // PVRATECONVERTER_H
