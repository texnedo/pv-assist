#ifndef PVMESSAGEOBSERVER_H
#define PVMESSAGEOBSERVER_H

#include "pvconcurentqueue.h"
#include <tr1/memory>
#include "pthread.h"

class PVMessage;

/*Abstract class-interface for objects which should have thread-safe message queue for interaction with
otther threads of application*/
class PVMessageObserver
{
protected:
    PVConcurentQueue<std::tr1::shared_ptr <PVMessage> > m_MsgQ;
    void WaitMessage();
    void WaitMessage(int iWaitTimeout);
    bool WaitMessageSafe();
    void CheckMessage();
    virtual void OnMessage( std::tr1::shared_ptr<PVMessage> inMsg) = 0;
    virtual void OnDestroy();
public:
    PVMessageObserver();
    void SendMessage(std::tr1::shared_ptr<PVMessage> inMsg);
    void SendMessageSync(std::tr1::shared_ptr<PVMessage> inMsg);
    virtual ~PVMessageObserver();
};

#endif // PVMESSAGEOBSERVER_H
