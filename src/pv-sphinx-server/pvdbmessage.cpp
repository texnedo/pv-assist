#include "pvdbmessage.h"
#include "./pvutils.h"

PVDBMessage::PVDBMessage(std::string tableName)
    :m_tableName(tableName)
{
    m_typeId = PVMessage::DB_TASK_MSG;
}

void PVDBMessage::AddParam(std::string name, std::string value)
{
    m_aQueryParams.insert(std::make_pair(name, value));
}

std::string PVDBMessage::GetFields()
{
    std::string res = "(";
    for (MAPSTR2STR::iterator it = m_aQueryParams.begin(); it != m_aQueryParams.end(); ++it)
    {
        if (res != "(")
            res += ",";
        res += it->first;
    }
    res += ")";
    return (res);
}

std::string PVDBMessage::GetValues()
{
    std::string res = "(";
    for (MAPSTR2STR::iterator it = m_aQueryParams.begin(); it != m_aQueryParams.end(); ++it)
    {
        if (res != "(")
            res += ",";
        if (it->second.size() != 0)
            res += "'" + it->second + "'";
        else
            res += "''";
    }
    res += ")";
    return (res);
}

PVDBMessage::~PVDBMessage()
{
}
