#include "pvsipclient.h"
#include "pvringbuffer.h"
#include "assert.h"
#include <tr1/memory>
#include "pvmessage.h"
#include "pvappconfig.h"
#include "pvutils.h"
#include "pvdbmessage.h"

extern PVAppConfig g_appConfig;

#define PJSIP_BUFFER_SIZE 1024
#define PJSIP_BUFFER_INCREMENT 1024

#define CLOCK_RATE	    8000
#define NCHANNELS	    1
#define SAMPLES_PER_FRAME   (NCHANNELS * (CLOCK_RATE * 10 / 1000))
#define BITS_PER_SAMPLE	    16

#define PJ_FAIL 2

//TODO - remove those file names to testmodules
//#define USE_RAW_VOICE_DUMP_FILE
//#define RAW_VOICE_DUMP_FILE "../test_data/sip_client_output8000.wav"

//#define USE_VOICE_ANSWER_FILE
#define USE_VOICE_ANSWER_MEMORY
//#define VOICE_ANSWER_FILE "/home/kvant/voice-answer.wav"

//#define USE_VOICE_ANSWER_MEMORY_FILE
//#define VOICE_ANSWER_MEMORY_FILE "../test_data/send_message3.wav"

SIPCLIENTMAP PVSipClient::clientMap;
pthread_mutex_t PVSipClient::clientMapMutex;

pthread_cond_t PVSipClient::connectionDestroyEvent;
pthread_mutex_t PVSipClient::connectionDestroyMutex;

//TODO - refactor ctor by dividing it in some functions
PVSipClient::PVSipClient(int iBufferSize, PVRingBuffer * lpOutputBuffer,
                         PVRingBuffer * lpInputBuffer,
                         PVMessageObserver *lpReceiver)
    :m_connectionThread(0),
     m_state(STATE_UNDEFINED),
     m_lpOutputExternalBuffer(lpOutputBuffer),
     m_lpInputExternalBuffer(lpInputBuffer),
     m_lpReceiver(lpReceiver)
{
    pj_status_t status;

    if (iBufferSize == 0)
    {
        PJ_LOG(1, (__FILE__, "Wrong buffer size provided"));
        throw (WRONG_BUFFER_SIZE);
    }

    m_iInternalBufferSize = iBufferSize;

    status = pjsua_create();
    //TODO - add pjsiplib wrapper and store it in auto_ptr, in case of exception in constructor it will be destroed automaticly
    if (status != PJ_SUCCESS)
    {
        PJ_LOG(1, (__FILE__, "Can't create pjsua"));
        throw (PJSIP_CREATE_ERROR);
    }

    m_config.pool = pjsua_pool_create("rectest", PJSIP_BUFFER_SIZE, PJSIP_BUFFER_INCREMENT);

    //init callbacks for dealing with events from pjsip
    m_config.media_cfg.clock_rate = CLOCK_RATE;
    m_config.cfg.cb.on_incoming_call = (void(*)(pjsua_acc_id acc_id, pjsua_call_id call_id,
                                                pjsip_rx_data *rdata))&PVSipClient::OnIncomingCall;

    m_config.cfg.cb.on_call_media_state = (void(*)(pjsua_call_id call_id))&PVSipClient::OnCallMediaState;

    m_config.cfg.cb.on_call_state = (void(*)(pjsua_call_id call_id,
                                             pjsip_event *e))&PVSipClient::OnCallState;

    m_config.cfg.cb.on_stream_created = (void(*)(pjsua_call_id call_id, pjmedia_stream *strm,
                                       unsigned stream_idx, pjmedia_port **p_port))&PVSipClient::OnStreamCreated;

    m_config.cfg.cb.on_reg_state2 = (void (*)(pjsua_acc_id acc_id, pjsua_reg_info *info))&PVSipClient::OnRegistrationStatus;

    //set log level
    pjsua_logging_config_default(&m_config.log_cfg);
    m_config.log_cfg.console_level = 4;

    status = pjsua_init(&m_config.cfg, &m_config.log_cfg, &m_config.media_cfg);
    if (status != PJ_SUCCESS)
    {
        PJ_LOG(1, (__FILE__, "Can't init pjsua"));
        throw (PJSIP_INIT_ERROR);
    }

    //prepare memory port for sound capturing
    m_config.mem_buffer = malloc(m_iInternalBufferSize);
    //TODO - move malloc and free to config constructor and destructor
    if (m_config.mem_buffer == NULL)
    {
        PJ_LOG(1, (__FILE__, "Can't init pjsua"));
        throw (MEMORY_ALLOCATION_ERROR);
    }

    status = pjmedia_mem_capture_create(m_config.pool,  m_config.mem_buffer, m_iInternalBufferSize,
                               CLOCK_RATE, 1, SAMPLES_PER_FRAME,
                               BITS_PER_SAMPLE, 0, &m_config.mem_port);
    if (status != PJ_SUCCESS)
    {
        PJ_LOG(1, (__FILE__, "Can't init mem writer port"));
        throw (MEM_WRITER_PORT_ERROR);
    }

    status = pjsua_set_null_snd_dev();


    std::string rvoiceFile = g_appConfig.GetParamValue(IO_RECEIVE_VOICE_DUMP);
    if (rvoiceFile.size() != 0)
        m_config.buffer_dump_file = fopen(rvoiceFile.c_str(), "w");

    std::string svoiceFile = g_appConfig.GetParamValue(IO_SEND_VOICE_DUMP);
    if (svoiceFile.size() != 0)
        m_config.play_mem_file = fopen(svoiceFile.c_str(), "r");


#ifdef USE_VOICE_ANSWER_FILE
    /* Load the WAV file to file port. */
    status = pjmedia_wav_player_port_create(
            m_config.pool,		    /* pool.	    */
            VOICE_ANSWER_FILE,  /* filename	    */
            0,		    /* use default ptime */
            0,		    /* flags	    */
            0,		    /* buf size	    */
            &m_config.play_mem_port	    /* result	    */
            );
    if (status != PJ_SUCCESS)
    {
        PJ_LOG(1, (__FILE__, "Can't init memory reader port"));
        throw (MEM_READER_PORT_ERROR);
    }
#elif defined USE_VOICE_ANSWER_MEMORY

    //prepare memory port for sound playing
    m_config.play_mem_buffer = malloc(m_iInternalBufferSize);
    //TODO - move malloc and free to config constructor and destructor
    if (m_config.play_mem_buffer == NULL)
    {
        PJ_LOG(1, (__FILE__, "Can't init pjsua"));
        throw (MEMORY_ALLOCATION_ERROR);
    }
    status = pjmedia_mem_player_create(m_config.pool, m_config.play_mem_buffer, m_iInternalBufferSize,
                                       CLOCK_RATE, 1, SAMPLES_PER_FRAME,
                                       BITS_PER_SAMPLE, 0, &m_config.play_mem_port);
    if (status != PJ_SUCCESS)
    {
        PJ_LOG(1, (__FILE__, "Can't init memory reader port"));
        throw (MEM_READER_PORT_ERROR);
    }
    status = pjmedia_mem_player_set_eof_cb(m_config.play_mem_port, &m_config.acc_id,
                                           (pj_status_t (*)(pjmedia_port *port, void *usr_data))&PVSipClient::OnPlayMemoryBufferEmpty);
    if (status != PJ_SUCCESS)
    {
        PJ_LOG(1, (__FILE__, "Can't init memory reader port callback"));
        throw (MEM_READER_PORT_ERROR);
    }
#endif

}

bool PVSipClient::AddVoiceData(char *data, int iSize)
{
    if (m_lpOutputExternalBuffer == NULL)
        return (false);

    return(m_lpOutputExternalBuffer->WriteData((char *)data, iSize));
}

bool PVSipClient::GetVoiceData(char *data, int iSize)
{
    if (m_lpInputExternalBuffer == NULL)
        return (false);

    return(m_lpInputExternalBuffer->ReadData((char *)data, iSize));
}

void PVSipClient::DestroyResources()
{
    //TODO - use timelimited join
    pthread_cond_signal(&connectionDestroyEvent);

    if (m_connectionThread)
        pthread_join(m_connectionThread, NULL);

    //remove callbacks from pjsip
    pthread_mutex_lock(&clientMapMutex);
    clientMap.erase(m_config.acc_id);
    pthread_mutex_unlock(&clientMapMutex);

    //then kill pjsip library
    pjsua_destroy();

    //destroy resources
    if (g_appConfig.HasParam(IO_RECEIVE_VOICE_DUMP))
        fclose(m_config.buffer_dump_file);

    if (g_appConfig.HasParam(IO_SEND_VOICE_DUMP))
        fclose(m_config.play_mem_file);

    free(m_config.mem_buffer);
    free(m_config.play_mem_buffer);
    pthread_mutex_destroy(&clientMapMutex);
    pthread_mutex_destroy(&connectionDestroyMutex);
}

void PVSipClient::SendDbNotifyMessage(std::string sMsg)
{
    if (m_lpReceiver == NULL)
        return;

    std::tr1::shared_ptr<PVDBMessage> msgDb(new PVDBMessage(DB_RESULTS_TABLE));

    msgDb->AddParam(DB_USERFROM_FIELD, g_appConfig.GetParamValue(SIP_USER_ARG));
    msgDb->AddParam(DB_USERTO_FIELD, g_appConfig.GetParamValue(SIP_PLACE_CALL_ARG));
    msgDb->AddParam(DB_PHRASE_FIELD, sMsg);

    char time [100];
    sprintf(time, "%d", msgDb->GetTimeStamp());
    msgDb->AddParam(DB_TIMESTAMP_FIELD, time);

    m_lpReceiver->SendMessage(msgDb);
}


PVSipClient::~PVSipClient()
{
    DestroyResources();
}

void PVSipClient::OnIncomingCall(pjsua_acc_id acc_id, pjsua_call_id call_id, pjsip_rx_data *rdata)
{
    pjsua_call_info ci;

    PJ_UNUSED_ARG(acc_id);
    PJ_UNUSED_ARG(rdata);

    pjsua_call_get_info(call_id, &ci);

    PJ_LOG(3,(__FILE__, "Incoming call from %.*s!!", (int)ci.remote_info.slen, ci.remote_info.ptr));

    pthread_mutex_lock(&clientMapMutex);
    SIPCLIENTMAP::iterator it = clientMap.find(ci.acc_id);
    pthread_mutex_unlock(&clientMapMutex);

    if (it == clientMap.end())
        return;

    PVSipClient * client = it->second;

    if (client->GetReceiver() != NULL)
    {
        if (client->GetState() == STATE_UNDEFINED)
        {
            /* Automatically answer incoming calls with 200/OK */
            pjsua_call_answer(call_id, 200, NULL, NULL);
        }
    }
}

void PVSipClient::OnCallState(pjsua_call_id call_id, pjsip_event *e)
{
    pjsua_call_info ci;

    PJ_UNUSED_ARG(e);

    pjsua_call_get_info(call_id, &ci);

    PJ_LOG(3,(__FILE__, "Call %d state=%.*s", call_id, (int)ci.state_text.slen, ci.state_text.ptr));

    pthread_mutex_lock(&clientMapMutex);
    SIPCLIENTMAP::iterator it = clientMap.find(ci.acc_id);
    pthread_mutex_unlock(&clientMapMutex);

    if (it == clientMap.end())
        return;

    PVSipClient * client = it->second;

    if (client->GetReceiver() != NULL)
    {
        switch(ci.state)
        {
        case PJSIP_INV_STATE_CONFIRMED:
            {
                //TODO - find more appropriate message type
                client->SetState(STATE_CONNECTED);
                if (!g_appConfig.HasParam(SIP_PLACE_CALL_ARG))
                {
                    std::tr1::shared_ptr<PVMessage> msg(new PVMessage(PVMessage::SIP_CONNECTED_MSG));
                    client->GetReceiver()->SendMessage(msg);
                }
                break;
            }
        case PJSIP_INV_STATE_DISCONNECTED:
            {
                client->SetState(STATE_DISCONNECTED);
                if (!g_appConfig.HasParam(SIP_PLACE_CALL_ARG))
                {
                    std::tr1::shared_ptr<PVMessage> msg(new PVMessage(PVMessage::SIP_DISCONNECTED_MSG));
                    client->GetReceiver()->SendMessage(msg);
                }
                else
                {
                    client->DestroyResources();
                }
                break;
            }
        default:
            {
            }
        }
    }
}

void PVSipClient::OnStreamCreated(pjsua_call_id call_id, pjmedia_stream *strm, unsigned stream_idx,
                                  pjmedia_port **p_port)
{

}

void PVSipClient::OnRegistrationStatus(pjsua_acc_id acc_id, pjsua_reg_info *info)
{

    PJ_LOG(3,(__FILE__, "Registration with accout id = %d", acc_id));

    pthread_mutex_lock(&clientMapMutex);
    SIPCLIENTMAP::iterator it = clientMap.find(acc_id);
    pthread_mutex_unlock(&clientMapMutex);

    if (it == clientMap.end())
        return;

    PVSipClient * client = it->second;

    if (client->GetReceiver() != NULL)
    {
        client->SendDbNotifyMessage("##REG##");
        if (info->cbparam->code == 200)
        {
            std::string destUri = g_appConfig.GetParamValue(SIP_PLACE_CALL_ARG);
            if (destUri.size() != 0)
                client->PlaceCall(strdup(destUri.c_str()));
        }
        else
        {
            PJ_LOG(3,(__FILE__, "Can't register with such credentials for account id = %d, code returned = %d", acc_id, info->cbparam->code));
            client->DestroyResources();
        }
    }
}

void PVSipClient::OnCallMediaState(pjsua_call_id call_id)
{
    pjsua_call_info ci;

    pjsua_call_get_info(call_id, &ci);

    //TODO - process hold state
    if (ci.media_status == PJSUA_CALL_MEDIA_ACTIVE)
    {
        pthread_mutex_lock(&clientMapMutex);
        SIPCLIENTMAP::iterator it = clientMap.find(ci.acc_id);
        pthread_mutex_unlock(&clientMapMutex);

        if (it == clientMap.end())
            return;

        //bind incoming data stream with memory buffer
        PVSipClient * client = it->second;
        PVSipClientConfig * conf = client->GetConfig();

        //TODO - refactor here and move port connections to if statement

        //port for writing incoming stream data into buffer
        pjsua_conf_add_port(conf->pool, conf->mem_port, &conf->mem_slot);
        pjsua_conf_connect(ci.conf_slot, conf->mem_slot);

        //port for preparing data for outgoing stream
        pjsua_conf_add_port(conf->pool, conf->play_mem_port, &conf->play_mem_slot);
        pjsua_conf_connect(conf->play_mem_slot, ci.conf_slot);

        if (client->GetState() == STATE_CONNECTED)
        {
            client->SetState(STATE_MEDIA_STARTED);
            //if we have external receiver send message to it
            if (client->GetReceiver() != NULL && !g_appConfig.HasParam(SIP_PLACE_CALL_ARG))
            {
                std::tr1::shared_ptr<PVMessage> msg(new PVMessage(PVMessage::SIP_MEDIA_STARTED_MSG));
                client->GetReceiver()->SendMessage(msg);
            }
            else
            {
                PJ_LOG(3,(__FILE__, "Sleep before first send"));
                sleep(10);
                client->SendDbNotifyMessage("##SND##");
            }
        }
    }

    PJ_LOG(3,(__FILE__, "Call %d media state=%d", call_id, ci.media_status));
}

pj_status_t PVSipClient::OnMemoryBufferFull(pjmedia_port *port, void *usr_data)
{
    if (port == NULL)
        return (PJ_FAIL);

    pj_size_t recorded_bytes = pjmedia_mem_capture_get_size (port);

    pjsua_acc_id * acc_id = (pjsua_acc_id *) usr_data;
    if (acc_id == NULL)
        return (PJ_FAIL);

    pthread_mutex_lock(&clientMapMutex);
    SIPCLIENTMAP::iterator it = clientMap.find(*acc_id);
    pthread_mutex_unlock(&clientMapMutex);

    if (it == clientMap.end())
        return (PJ_FAIL);

    PVSipClientConfig * conf = it->second->GetConfig();

#ifdef RICH_TRACE
    printf("%s%d\n", "Bytes recorded in memory = ", recorded_bytes);

    short * data = (short *)conf->mem_buffer;
    int i;
    for (i = 0; i < recorded_bytes / 2; i++, data++)
    {
        short data_val = *data;
        printf("%d\n", data_val);
    }

    printf("%s\n\n", "--------------------------------------");
    fflush(stdout);
#endif

    it->second->AddVoiceData((char *)conf->mem_buffer, recorded_bytes);

    if (g_appConfig.HasParam(IO_RECEIVE_VOICE_DUMP))
        fwrite(conf->mem_buffer, 2, recorded_bytes/2, conf->buffer_dump_file);

    return(PJ_SUCCESS);
}

pj_status_t PVSipClient::OnPlayMemoryBufferEmpty(pjmedia_port *port, void *usr_data)
{
    if (port == NULL)
        return (PJ_FAIL);

    //TODO - move this to separate function
    pjsua_acc_id * acc_id = (pjsua_acc_id *) usr_data;
    if (acc_id == NULL)
        return (PJ_FAIL);

    pthread_mutex_lock(&clientMapMutex);
    SIPCLIENTMAP::iterator it = clientMap.find(*acc_id);
    pthread_mutex_unlock(&clientMapMutex);

    if (it == clientMap.end())
        return (PJ_FAIL);

    PVSipClientConfig * conf = it->second->GetConfig();

    if (!g_appConfig.HasParam(IO_SEND_VOICE_DUMP))
    {
        if (it->second->GetVoiceData((char *)conf->play_mem_buffer,
                                     it->second->GetBufferSize()) != 0)
            return(PJ_SUCCESS);
        else
            return (PJ_FAIL);
    }
    else
    {
        int result = fread((char *)conf->play_mem_buffer, 1, it->second->GetBufferSize(), conf->play_mem_file);
        if (result != it->second->GetBufferSize())
        {
            sleep(15);
            it->second->SendDbNotifyMessage("##SND##");
            if (fseek(conf->play_mem_file, 0, SEEK_SET) != 0)
                return (PJ_FAIL);
            else
                return(PJ_SUCCESS);
        }
        else
            return (PJ_FAIL);
    }  
}

void *PVSipClient::ConnectionThreadProc(void * lpSipClient)
{
    PVSipClient * sipClient = (PVSipClient *)lpSipClient;
    if (sipClient == NULL)
        return (NULL);

    if (!sipClient->ConnectInternal())
    {
        sipClient->DestroyResources();
        return (NULL);
    }

    //TODO -check return values
    pthread_mutex_lock(&connectionDestroyMutex);
    pthread_cond_wait(&connectionDestroyEvent, &connectionDestroyMutex);
    pthread_mutex_unlock(&connectionDestroyMutex);

    return (NULL);
}

bool PVSipClient::ConnectInternal()
{
    pj_status_t status;

    pj_thread_register("PVSipClientThread", m_pjsipThreadDesc, &m_pjsipThread);

    /* Add UDP transport. */
    pjsua_transport_config_default(&m_config.udp_cfg);


    std::string strPort = g_appConfig.GetParamValue(SIP_PORT);
    if (strPort.size() == 0 || !pvutils::isdigit(strPort))
        return (false);

    m_config.udp_cfg.port = atoi(strPort.c_str());
    status = pjsua_transport_create(PJSIP_TRANSPORT_UDP, &m_config.udp_cfg, NULL);
    if (status != PJ_SUCCESS)
        return (false);

    /* Initialization is done, now start pjsua */
    status = pjsua_start();
    if (status != PJ_SUCCESS)
        return (false);

    /* Register to SIP server by creating SIP account. */
    pjsua_acc_config_default(&m_config.acc_cfg);

    std::string sipUser = g_appConfig.GetParamValue(SIP_USER_ARG);
    std::string sipDomain = g_appConfig.GetParamValue(SIP_DOMAIN_ARG);
    std::string sipPassword = g_appConfig.GetParamValue(SIP_PASSWORD_ARG);
    if (sipUser.size() == 0 || sipDomain.size() == 0 || sipPassword.size() == 0)
        throw(WRONG_CREDENTIALS);

    std::string sipAccount = std::string("sip:") + sipUser + "@" + sipDomain;
    std::string sipRegUri = std::string("sip:" + sipDomain);
    m_config.acc_cfg.id = pj_str(strdup(sipAccount.c_str()));
    m_config.acc_cfg.reg_uri = pj_str(strdup(sipRegUri.c_str()));
    m_config.acc_cfg.cred_count = 1;
    //m_config.acc_cfg.cred_info[0].realm = pj_str(SIP_DOMAIN);
    m_config.acc_cfg.cred_info[0].realm = pj_str(strdup("asterisk"));
    m_config.acc_cfg.cred_info[0].scheme = pj_str(strdup("digest"));
    m_config.acc_cfg.cred_info[0].username = pj_str(strdup(sipUser.c_str()));
    m_config.acc_cfg.cred_info[0].data_type = PJSIP_CRED_DATA_PLAIN_PASSWD;
    m_config.acc_cfg.cred_info[0].data = pj_str(strdup(sipPassword.c_str()));

    status = pjsua_acc_add(&m_config.acc_cfg, PJ_TRUE, &m_config.acc_id);
    if (status != PJ_SUCCESS)
        return (false);

    pjmedia_mem_capture_set_eof_cb(m_config.mem_port, &m_config.acc_id,
                (pj_status_t (*)(pjmedia_port *port, void *usr_data))&PVSipClient::OnMemoryBufferFull);

    clientMap.insert(std::make_pair(m_config.acc_id, this));

    return (true);
}

bool PVSipClient::PlaceCall(char *destUri)
{
    if (destUri == NULL)
        return (false);

    pj_str_t uri = pj_str(destUri);
    pj_status_t status = pjsua_call_make_call(m_config.acc_id, &uri, 0, NULL, NULL, NULL);

    if (status != PJ_SUCCESS)
    {
        PJ_LOG(3,(__FILE__, "Can't make call for account id = %d to contact = %s", m_config.acc_id, destUri));
        DestroyResources();
        return (false);
    }
    else
        return (true);
}

bool PVSipClient::Connect()
{
    if (pthread_cond_init(&connectionDestroyEvent, NULL))
        return (false);

    if (pthread_mutex_init(&connectionDestroyMutex, NULL))
        return (false);

    if (pthread_mutex_init(&clientMapMutex, NULL))
        return (false);

    if (pthread_create( &m_connectionThread, NULL, (void *(*)(void *))&ConnectionThreadProc, this) != 0)
        return (false);

    return (true);
}
