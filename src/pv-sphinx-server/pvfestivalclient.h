#ifndef PVFESTIVALCLIENT_H
#define PVFESTIVALCLIENT_H

#include <string>
#include <vector>

#define FESTIVAL_DEFAULT_SERVER_HOST "localhost"
#define FESTIVAL_DEFAULT_SERVER_PORT 1314
#define FESTIVAL_DEFAULT_TEXT_MODE "fundamental"

struct FT_Info
{
    int encoding;
    char *server_host;
    int server_port;
    char *text_mode;

    int server_fd;
};

class PVFestivalClient
{
private:
    FT_Info * m_Info;

    void DeleteFTInfo(FT_Info *info);
    void SocketReceive2Buffer(int fd, std::vector<char> & outputBuffer);
    FT_Info *CreateDefaultInfo();
    void ClientAcceptExpr(int fd, std::vector<char> &buffer);
    int FestivalSocketOpen(const char *host, int port);
    bool Close();
public:
    PVFestivalClient();
    ~PVFestivalClient();
    bool Open(FT_Info *info = NULL);
    bool StringToWave(std::string sText, std::vector<char> &outputBuffer);
};

#endif // PVFESTIVALCLIENT_H
