#!/bin/sh

# This is a simple example decoder
# Please note, that recognize the file should be single-channel with sample rate 8kHz
# decoder-test.wav: RIFF (little-endian) data, WAVE audio, Microsoft PCM, 16 bit, mono 8000 Hz
# Expected result is: ил -ья ильф евген -ий петр -ов золот -ой тел -ёнок

pocketsphinx_continuous \
    -samprate 8000 \
    -lm zero_ru.lm \
    -dict zero_ru.dic \
    -hmm zero_ru_cd_cont_2000 \
    -infile decoder-test.wav


