#! /bin/bash

sh ./pocketsphinx_continuous \
-fwdflat no \
-bestpath no \
-lm /usr/share/pocketsphinx/model/lm/tidigits/tidigits.lm \
-dict /usr/share/pocketsphinx/model/lm/tidigits/tidigits.dic \
-hmm /usr/share/pocketsphinx/model/hmm/tidigits \
-samprate 8000 \
-nfft 256 \
-infile /home/kvant/asterisk-voice-dump.wav
