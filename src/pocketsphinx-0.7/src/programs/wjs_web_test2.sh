#! /bin/bash

sh ./pocketsphinx_continuous \
-fwdflat no \
-bestpath no \
-samprate 8000 \
-lm /usr/share/pocketsphinx/model/lm/web_test2/2114.lm \
-dict /usr/share/pocketsphinx/model/lm/web_test2/2114.dic \
-nfft 256 \
-infile /home/kvant/asterisk-voice-dump.wav 
