#! /bin/bash

sh ./pocketsphinx_continuous \
-fwdflat no \
-bestpath no \
-dict /usr/share/pocketsphinx/model/lm/ruvoxforge/msu_ru_nsh.dic \
-hmm /usr/share/pocketsphinx/model/hmm/ruvoxforge \
-samprate 8000 \
-nfft 256 \
-infile /home/kvant/asterisk-voice-dump.wav
